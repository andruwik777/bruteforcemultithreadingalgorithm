import sys
import time
import traceback
from threading import Thread
from threading import Condition

ATTEMPTS_COUNT = 3
CORRECT_PASSWORD = 500
CHECK_PASSWORD_DELAY = 0.01
GENERATE_BUFFER_DELAY = 0.01
MATHEMATICS_DELAY = 0.01
BLOCK_SIZE = 5

DELAY_BEFORE_EXIT = 5

EXIT_FLAG = "EXIT"
ALLOW_ADDITIONAL_MEMORY = True

cond = Condition()
def myPrint(info):
    with cond:
        print (info)

class RequestLocks():
    def __init__(self, rq1, rq2):
        self.rq1 = rq1
        self.rq2 = rq2
        
class ResponseLocks():
    def __init__(self, rs1, rs2):
        self.rs1 = rs1
        self.rs2 = rs2

class Locks():
    def __init__(self, requestLocks, responseLocks):
        self.requestLocks = requestLocks
        self.responseLocks = responseLocks

def initLocks():
    requestLock1 = Condition()
    requestLock2 = Condition()
    responseLock1 = Condition()
    responseLock2 = Condition()

    rq = RequestLocks(requestLock1, requestLock2)
    rs = ResponseLocks(responseLock1, responseLock2)
    return Locks(rq, rs)
    
    
class RequestBlocks():
    def __init__(self, rq1, rq2):
        self.rq1 = rq1
        self.rq2 = rq2
        
class ResponseBlocks():
    def __init__(self, rs1, rs2):
        self.rs1 = rs1
        self.rs2 = rs2  
        
class Blocks():
    def __init__(self, requestBlock, responseBlock):
        self.requestBlock = requestBlock
        self.responseBlock = responseBlock
        
def initBlocks():
    requestBlock1 = list()
    requestBlock2 = list()
    responseBlock1 = list()
    responseBlock2 = list()
    
    rq = RequestBlocks(requestBlock1, requestBlock2)
    rs = ResponseBlocks(responseBlock1, responseBlock2)
    return Blocks(rq, rs)
    
 
def getHash(requestBlock, responseBlock):
    time.sleep(MATHEMATICS_DELAY)
    for item in requestBlock:
        responseBlock.append(item)
 
def processBlock(requestBlock, requestLock, responseBlock, responseLock):
    requestLock.acquire()

    if not requestBlock: 
        requestLock.wait()
    requestLock.release()
    
    if requestBlock[-1] == EXIT_FLAG:
        return True
    
    responseLock.acquire()
    getHash(requestBlock, responseBlock)
    del requestBlock[:]
    responseLock.notify()
    responseLock.release()
    return False
    
def performMathematics(blocks, locks):
    evenCounter = 0
    
    while True:
        evenCounter += 1
        needExit = False
        if not ALLOW_ADDITIONAL_MEMORY or not evenCounter % 2 == 0:
            needExit = processBlock(blocks.requestBlock.rq1,
                                    locks.requestLocks.rq1,
                                    blocks.responseBlock.rs1,
                                    locks.responseLocks.rs1)            
        else:
            needExit = processBlock(blocks.requestBlock.rq2,
                                    locks.requestLocks.rq2,
                                    blocks.responseBlock.rs2,
                                    locks.responseLocks.rs2)
        if needExit:
            break
                         
def generateNextBlock(uncheckedPasswordValue, block):
    time.sleep(GENERATE_BUFFER_DELAY)
    newUncheckedPasswordValue = uncheckedPasswordValue + BLOCK_SIZE
    for num in range(uncheckedPasswordValue, newUncheckedPasswordValue):
        block.append(num)
    return newUncheckedPasswordValue
    
def generateBlockAndSendToMathThread(uncheckedPasswordValue, block, lock):
    lock.acquire()
    uncheckedPasswordValue = generateNextBlock(uncheckedPasswordValue, block)
    lock.notify()
    lock.release()
    return uncheckedPasswordValue
    
def checkPassword(block):
    #myPrint ("Check " + str(block[0]) + "..." + str(block[-1]))
    time.sleep(CHECK_PASSWORD_DELAY)
    for item in block:
        if item == CORRECT_PASSWORD:
            return item
    return None
    
def checkAndGenerateNext(requestBlock, requestLock, responseBlock, responceLock, uncheckedPasswordValue):
    responceLock.acquire()
    if not responseBlock:
        responceLock.wait()
    responceLock.release()
    
    password = checkPassword(responseBlock)
    del responseBlock[:]
    if password:
        return password, 0
    else:
        newUncheckedPasswordValue = generateBlockAndSendToMathThread(uncheckedPasswordValue,
                                                                     requestBlock,
                                                                     requestLock)
        return None, newUncheckedPasswordValue
                     
def stopMathThread(block, lock):
    lock.acquire()
    block.append(EXIT_FLAG)
    lock.notify()
    lock.release()
                     
def bruteforcePassword(blocks, locks):
    uncheckedPasswordValue = 0
    evenCounter = 0
    password = None
    
    uncheckedPasswordValue = generateBlockAndSendToMathThread(uncheckedPasswordValue,
                                                              blocks.requestBlock.rq1,
                                                              locks.requestLocks.rq1)
    if ALLOW_ADDITIONAL_MEMORY:
        uncheckedPasswordValue = generateBlockAndSendToMathThread(uncheckedPasswordValue,
                                                                  blocks.requestBlock.rq2,
                                                                  locks.requestLocks.rq2)
    
    while True:
        evenCounter += 1
        if not ALLOW_ADDITIONAL_MEMORY or not evenCounter % 2 == 0:
            password, uncheckedPasswordValue = checkAndGenerateNext(blocks.requestBlock.rq1,
                                                                    locks.requestLocks.rq1,
                                                                    blocks.responseBlock.rs1,
                                                                    locks.responseLocks.rs1,
                                                                    uncheckedPasswordValue)
        else:
            password, uncheckedPasswordValue = checkAndGenerateNext(blocks.requestBlock.rq2,
                                                                    locks.requestLocks.rq2,
                                                                    blocks.responseBlock.rs2,
                                                                    locks.responseLocks.rs2,
                                                                    uncheckedPasswordValue)
        if password:
            stopMathThread(blocks.requestBlock.rq1, locks.requestLocks.rq1)
            stopMathThread(blocks.requestBlock.rq2, locks.requestLocks.rq2)
            break
    return password
    
def findPassword():
    blocks = initBlocks()
    locks = initLocks()
    
    t = Thread(target=performMathematics, args=((blocks, locks)))
    t.start()
    
    pwd = bruteforcePassword(blocks, locks)
    myPrint("Password:" + str(pwd))
    
def findPasswordSinglethread(initialValue, isFound):
    uncheckedPasswordValue = initialValue
    passwordsBlock = list()
    hashsBlock = list()
    password = None
    while not password:
        if isFound[0]:
            return
        del passwordsBlock[:]
        del hashsBlock[:]
        uncheckedPasswordValue = generateNextBlock(uncheckedPasswordValue, passwordsBlock)
        getHash(passwordsBlock, hashsBlock)
        password = checkPassword(hashsBlock)
    isFound[0] = True
    myPrint("Password:" + str(password))

def findPasswordSeries():
    isFound = [False]
    t = Thread(target=findPasswordSinglethread, args=((CORRECT_PASSWORD/2, isFound)))
    t.start()
    findPasswordSinglethread(0, isFound)
    t.join()

   
def oneAttempt(function, *args):
    start_time = time.time()
    function(*args)
    stop_time = time.time()
    return stop_time - start_time
    
def seriesOfAttempts(function, *args):
    time = 0
    for c in range(ATTEMPTS_COUNT):
        time += oneAttempt(function, *args)
    return round(time/ATTEMPTS_COUNT, 2)
    
def main():
    global ALLOW_ADDITIONAL_MEMORY
    ALLOW_ADDITIONAL_MEMORY = False
    timeWithoutMemory = seriesOfAttempts(findPassword)
    myPrint('{:<45}  {:<8}  {:<7}'.format
        ("Average time WITHOUT memory block:", "%s sec" % (timeWithoutMemory), "~ 100%"))
    
    ALLOW_ADDITIONAL_MEMORY = True
    timeWithMemory = seriesOfAttempts(findPassword)
    myPrint('{:<45}  {:<8}  {:<7}'.format
        ("Average time WITH memory block:", "%s sec" % (timeWithMemory), "~ %s%%" % (int((timeWithMemory*100)/timeWithoutMemory))))    
    
    timeSeries = seriesOfAttempts(findPasswordSeries)
    myPrint('{:<45}  {:<8}  {:<7}'.format
        ("Average time with 2 SERIES EXECUTION threads:", "%s sec" % (timeSeries), "~ %s%%" % (int((timeSeries*100)/timeWithoutMemory))))
    
if __name__ == "__main__":
    try:
        main()
        time.sleep(DELAY_BEFORE_EXIT)
    except Exception as e:
        traceback.print_exc()
